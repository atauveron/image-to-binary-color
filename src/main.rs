use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use clap::{App, Arg};
use image::imageops::FilterType;

const DEFAULT_WIDTH: u32 = 48;
const DEFAULT_HEIGHT: u32 = 48;

fn main() {
    let matches = App::new("image-to-binary-color")
        .author("Aimery Tauveron <atauveron@via.ecp.fr>")
        .version("0.1.0")
        .about("A command-line utility to convert images to `BinaryColor`-compatible raw bytes")
        .arg(
            Arg::with_name("width")
                .long("width")
                .value_name("WIDTH")
                .help("Width of the target image (in pixels)")
                .required(false),
        )
        .arg(
            Arg::with_name("height")
                .long("height")
                .value_name("HEIGHT")
                .help("Height of the target image (in pixels)")
                .required(false),
        )
        .arg(
            Arg::with_name("files")
                .value_name("FILE")
                .help("Files to convert")
                .multiple(true)
                .min_values(1)
                .required(true),
        )
        .get_matches();

    let width = matches
        .value_of("width")
        .map_or(DEFAULT_WIDTH, |s| s.parse::<u32>().unwrap_or(DEFAULT_WIDTH));
    let height = matches.value_of("height").map_or(DEFAULT_HEIGHT, |s| {
        s.parse::<u32>().unwrap_or(DEFAULT_HEIGHT)
    });

    if (width * height) % 8 != 0 {
        eprintln!("The output size (in pixels) must be a multiple of 8");
        std::process::exit(1);
    }

    let filenames = matches.values_of_os("files").unwrap();

    for filename in filenames {
        let filepath = Path::new(filename);
        let output_path = filepath.with_extension("bin");

        let image = convert(filepath, width, height);
        save_as_file(output_path.as_path(), image.as_slice());
    }
}

fn convert(filepath: &Path, width: u32, height: u32) -> Vec<u8> {
    println!("Processing image file {}", filepath.to_str().unwrap());
    // Open image and convert to grayscale
    let image = image::open(filepath).expect("Failed to open image");
    let gray_image = image
        .resize_exact(width, height, FilterType::Triangle)
        .grayscale()
        .into_luma();
    // Copy to byte array
    let mut res: Vec<u8> = vec![0; (gray_image.width() * gray_image.height() / 8) as usize];
    for (index, value) in gray_image.iter().enumerate() {
        let i = index / 8;
        let offset = index % 8;
        let bit: u8 = if value < &200 { 1 } else { 0 };
        res[i] |= bit << (7 - offset);
    }
    // Return
    res
}

fn save_as_file(filepath: &Path, buf: &[u8]) -> () {
    println!("Saving to {}", filepath.to_str().unwrap());
    let mut file = File::create(filepath).expect("Failed to create output file");
    file.write_all(buf).expect("Failed to write to output file");
}
