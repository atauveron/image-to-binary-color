# image-to-binary-color

A command-line utility to convert images to `BinaryColor`-encoded files

## Usage

This CLI utility reads image files (from the provided paths). It then resizes the images (default size is 48 by 48 pixels), converts them first to grayscale then to black and white, then saves the data to a file (using `BinaryColor` encoding).

The output file is written in the same directory as the corresponding input file, it uses the same file name (with a `bin` extension).

## `BinaryColor` encoding

The `BinaryColor` encoding is simple :

* a pixel is represented by a single bit,
* a bit value of 0 represents a white pixel,
* a bit value of 1 represents a black pixel.

No compression is used, no metadata is added. Only the raw pixel data is saved.
